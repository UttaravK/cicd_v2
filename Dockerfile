FROM openjdk:8
VOLUME /tmp
ADD /target/*.jar WebApp2.jar
EXPOSE 8085
CMD ["java", "-jar", "/WebApp2.jar"]
